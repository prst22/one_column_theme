<?php
	get_header();
?>

<main class="container main">
	<div class="row">
		<div class="col-12 position-relative">
        <?php if(is_paged()): ?>
			<div class="load_more_cnt load_more_cnt--previous">
				<span class="mounttop">This is the top</span>
		    	<button id="load_previous" class="load_button" data-prev="1" data-number="<?php echo if_paged(1); ?>" data-url="<?php echo admin_url('admin-ajax.php'); ?>">LOAD PREVIOUS</button>
		    </div>
        <?php endif; ?>
			<div class="posts_cnt <?php if(!is_paged()){echo "padding_t";} ?>">
				<?php echo '<div data-number="1" class="post_item" data-page="' . get_site_url(null , null , 'relative') . '/'. if_paged() .'">'; ?>
					<?php 
					if ( have_posts() ) {

						while ( have_posts() ) {
							
							the_post();					        
							get_template_part( 'template-parts/one', 'post' ); 
				        	
						} // end while
					} // end if
					?>

				<?php echo '</div>'; ?>
			</div>
			
		    <div class="load_more_cnt">
		    	<span class="rocbot">Rock Bottom</span>
		    	<button id="load_posts" class="load_button" data-number="<?php echo if_paged(1); ?>" data-url="<?php echo admin_url('admin-ajax.php'); ?>">LOAD MORE</button>
		    </div>
	    </div>
    </div>
</main>

<?php		
	get_footer();
?>