<?php
/**
 * The template for displaying search results pages
 */

get_header(); ?>
<?php
	global $wp_query;
	$total_results = $wp_query->found_posts;
?>
<main class="container main search_results_page">
        <div class="row">
            <div class="col-12">
                
                <div class="text-center search_results_page__form"><?php get_search_form(); ?></div>
                <header>
	                <h1 class="text-center search_header">
						<?php printf( __( 'Search Results for: %s', 'oc_theme' ), get_search_query() ); ?>
						<?php echo $total_results; ?>
					</h1>
				</header>
            	<?php if(is_paged()): ?>
					<div class="load_more_cnt load_more_cnt--previous">
						<span class="mounttop">This is the top</span>
				    	<button id="load_previous" class="load_button" data-prev="1" data-number="<?php echo if_paged(1); ?>" data-url="<?php echo admin_url('admin-ajax.php'); ?>" data-search="<?php echo get_search_query(); ?>
				">LOAD PREVIOUS</button>
				    </div>
		        <?php endif; ?>

				<div class="results single_page">
					
					<?php if ( have_posts() ) : ?>

							<!-- .page-header -->

								<?php echo '<div data-number="1" class="post_item" data-page="' . get_site_url(null , null , 'relative') . '/'. if_paged() .'?s='. get_search_query() .'&submit='.'">'; 
								while ( have_posts() ) :
									the_post();
									get_template_part( 'template-parts/one', 'post' ); 
								endwhile;
								echo '</div>';
								
						else :
							echo '<div data-number="1" class="post_item" data-page="' . get_site_url(null , null , 'relative') . '/'. if_paged() .'?s='. get_search_query() .'&submit='.'">'; ?>
							<h1 class="text-center mt-5 mb-3">No post found</h1>
							<?php
							echo '</div>';
						endif;
					?>

		        </div>
		        
		        <?php if ( have_posts() ) : ?>
					<div class="load_more_cnt">
				    	<span class="rocbot">Rock Bottom</span>
				    	<button id="load_posts" class="load_button" data-number="<?php echo if_paged(1); ?>" data-url="<?php echo admin_url('admin-ajax.php'); ?>" data-search="<?php echo get_search_query(); ?>
						">LOAD MORE</button>
				    </div>
				<?php endif; ?>
		
		</div>
	</div>
</main>
<?php get_footer();
