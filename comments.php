<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 * @since 1.0
 * @version 1.0
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */

?>

<div id="comments" class="comments-area">
    <?php
    // You can start editing here -- including this comment!
    if ( have_comments() ) : ?>
        <h2 class="comments-title">
            <?php
            $comments_number = get_comments_number();
            if ( '1' === $comments_number ) {
                /* translators: %s: post title */
                printf( _x( 'One Reply to &ldquo;%s&rdquo;', 'comments title', 'oc-theme' ), get_the_title() );
            } else {
                printf(
                    /* translators: 1: number of comments, 2: post title */
                    _nx(
                        '%1$s Reply to &ldquo;%2$s&rdquo;',
                        '%1$s Replies to &ldquo;%2$s&rdquo;',
                        $comments_number,
                        'comments title',
                        'oc-theme'
                    ),
                    number_format_i18n( $comments_number ),
                    get_the_title()
                );
            }
            ?>
        </h2>

        <ul class="comment-list">
            <?php
                wp_list_comments( array(
                    'avatar_size' => 100,
                    'style'       => 'ul',
                    'short_ping'  => true,
                    'reply_text'  => 'Reply',
                ) );
            ?>
        </ul>
        <?php the_comments_pagination( array(
            'prev_text' => '<span class="comments_icon"><svg class="icon icon-chevron-left"><use xlink:href="'. get_template_directory_uri() . '/symbol-defs.svg#icon-chevron-left"></use></svg></span>',
            'next_text' => '<span class="comments_icon"><svg class="icon icon-chevron-right"><use xlink:href="'. get_template_directory_uri() . '/symbol-defs.svg#icon-chevron-right"></use></svg></span>',
        ) );
    endif; // Check for have_comments().

    // If comments are closed and there are comments, let's leave a little note, shall we?
    if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) : ?>

        <p class="no-comments"><?php _e( 'Comments are closed.', 'oc-theme' ); ?></p>

    <?php
    endif;

    comment_form();
    ?>

</div><!-- #comments -->
