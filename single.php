<?php

get_header(); ?>
    <main id="main" class="container single_post_main">
        
        <?php
        // Start the loop.
        while ( have_posts() ) : the_post();
            save_post_views(get_the_ID());
            get_template_part( 'template-parts/content', get_post_format() );
 
            // If comments are open or we have at least one comment, load up the comment template.
            ?>
            <div class="row">
                    <div class="col-12 pt-3">
                        <?php 
                        // Previous/next post navigation.
                            the_post_navigation( array(
                                'next_text' => '<span class="meta-nav" aria-hidden="true" hidden>' . __( 'Next', 'oc-theme' ) . '</span> ' .
                                    '<span class="screen-reader-text">' . __( 'Next post:', 'oc-theme' ) . '</span> ' .
                                    '<span class="post-title">%title</span>
                                    <svg class="pl-1 icon icon-chevron-right"><use xlink:href="'. get_template_directory_uri() . '/symbol-defs.svg#icon-chevron-right"></use></svg>',
                                'prev_text' => '<span class="meta-nav" aria-hidden="true" hidden>' . __( 'Previous', 'oc-theme' ) . '</span> ' .
                                    '<span class="screen-reader-text"><svg class="pr-2 icon icon-chevron-left"><use xlink:href="'. get_template_directory_uri() . '/symbol-defs.svg#icon-chevron-left"></use></svg>' . __( 'Previous post:', 'oc-theme' ) . '</span> ' .
                                    '<span class="post-title">%title</span>',
                            ) );
                        ?>
                    </div>
            </div>
            <?php
            if ( comments_open() ) :
                comments_template();
            endif;
            
        // End the loop.
        endwhile;
        ?>
    </main><!-- .site-main -->
<?php get_footer(); ?>