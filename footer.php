		<div class="container">
			<div class="row">
				<footer class="col-12 footer">
					<div class="footer__inner">
						<?php  if( is_active_sidebar('footer_widget') || has_nav_menu( 'footer_menu' ) ):?>

							<div class="my-3 d-flex justify-content-center justify-content-sm-between align-items-center flex-column flex-sm-row footer_content">

								<?php if ( has_nav_menu( 'footer_menu' ) ) : ?>
									<nav class="footer_content__footer_menu">
										<?php
										$args = array(
											'theme_location' => 'footer_menu'
										);
										wp_nav_menu($args); ?>

									</nav>
								<?php endif; ?>

								<?php
									if(is_active_sidebar('footer_widget')){ 
								            dynamic_sidebar('footer_widget');
							            }
								?>

	                        </div>
	                    <?php endif; ?>
						<div class="about" id="footer">
							<h4 class="about__header">About</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
							quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
							consequat.</p>	
						</div>
	                    
			        </div>

					<div class="bottom__footer-copy">
						<span class="footer_copy_icon">
							<svg class="icon icon-copyright"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-copyright"></use></svg>
						</span>
						
						<?php echo date("Y") ;?> - <a href="http://www.potatosites.com/">potatosites.com</a>	
					</div> 
					
				</footer>
			</div>
		</div>
		<?php if ( !have_comments() ) : ?> 
			<div class="gradient gradient--bottom"></div>
		<?php endif; ?>

	</div>
	
</div>
<div class="go_top">
	<span class="go_top__icon hidden_g">
		<svg class="icon icon-circle-up"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-circle-up"></use></svg>
	</span>
</div>
<?php wp_footer(); ?>
</body>

</html>
