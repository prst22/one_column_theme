let Scrollbar = window.Scrollbar,
mainCntScroll,
scrollCnt = document.querySelector('.site_wrap'),
postReveal = {},
lastScroll = 0,
contactForm = document.getElementById('one_column_cf'),
searchOpenBtn = document.querySelectorAll('a[href^="#open_search"]'),
searchCloseBtn = document.getElementById('close_search'),
searchCnt = document.getElementById('search_block'),
mobileMenuToggle = document.getElementById('mobile_menu_btn'),
mobileMenuClose = document.getElementById('close_menu'),
mobileMenu = document.querySelector('.mobile_menu_cnt'),
scrollbarMenuCnt = document.querySelector('.mobile_menu_cnt__inner'),
goTopBtn = document.querySelector('.go_top__icon'),
menuColumn,
postPage = document.getElementsByClassName('post_item');
Scrollbar.use(window.OverscrollPlugin);
if(mobileMenu) {
	menuColumn = mobileMenu.querySelector('.mobile_menu_cnt__inner');
}

window.addEventListener("load",() =>{
	mainCntScroll = Scrollbar.init(scrollCnt, ScrollbarOptions = {
	    damping: 0.05,
	    plugins: {
		    overscroll: true
	    }
	});
	mainCntScroll.updatePluginOptions('overscroll', {
		effect: 'bounce',
	    maxOverscroll: 180
	});
	document.getElementsByClassName('pre_loader')[0].classList.add('hidden');
	if(document.body.classList.contains('home') 
		||  document.body.classList.contains('search') 
		||  document.body.classList.contains('archive')){
		let btn = document.getElementsByClassName('load_button'),
		firstPage = document.querySelector('div[data-number^="1"]:not(.added)');
		for(i = 0; i < btn.length; i++){
			btn[i].addEventListener('click', e => {postReveal.requestPosts(e)});
		}
		if(document.querySelector('.load_more_cnt')){
			postReveal.postCnt = document.querySelector('.load_more_cnt').parentNode;
		}
		firstPage.classList.add('added');
	}
    
    mainCntScroll.setPosition(0, 0);
    mainCntScroll.update();
	if(document.body.classList.contains('single')){
        document.querySelector('.single_post:not(.added)').classList.add('added');
	}
    if(document.body.classList.contains('page')){
    	let page = document.querySelector('.single_page_to_animate:not(.revealed)');
    	if(page) page.classList.add('revealed');
    }
});

postReveal.appendLoader = function(){
	let loader = document.createElement('DIV'),
	spinner = document.createElement('DIV');
	loader.classList.add('loading','loading--bottom');
	spinner.classList.add('simple-spinner');
	loader.appendChild(spinner);
	return loader;
},
postReveal.appendLoaderTop = function(){
	let loader = document.createElement('DIV'),
	spinner = document.createElement('DIV');
	loader.classList.add('loading','loading--top');
	spinner.classList.add('simple-spinner');
	loader.appendChild(spinner);
	return loader;
},

postReveal.requestPosts = function(event){
	event.target.disabled = true;
	const ajaxUrl = event.target.getAttribute('data-url');
	let dataNumber = (!isNaN( event.target.getAttribute('data-number'))) ? event.target.getAttribute('data-number') : 1,
	dataPrev = (!isNaN( event.target.getAttribute('data-prev'))) ? event.target.getAttribute('data-prev') : 1,
	search = event.target.getAttribute('data-search'),
	archive = event.target.getAttribute('data-archive'),
	sentCnt = document.querySelector('.load_more_cnt'),
	bottomNotification = document.querySelector('.rocbot'),
	topNotification = document.querySelector('.mounttop');
	formData = new FormData();

	if(dataPrev === null) dataPrev = 0;
	if(search === null) search = 0;
	if(archive === null) archive = 0;

	formData.append('number', dataNumber);
	formData.append('prev', dataPrev);
	formData.append('search', search);
	formData.append('archive', archive);
	formData.append('action', 'first_fetch_f');
    
    let options = {
    	method: 'POST',
    	body: formData
    };

    if(event.target.getAttribute('id') == 'load_previous'){
    	postReveal.postCnt.insertBefore(postReveal.appendLoaderTop(), sentCnt);
    }else{
    	postReveal.postCnt.insertBefore(postReveal.appendLoader(), sentCnt);
    }
	let req = new Request(ajaxUrl, options);

	fetch(req)
		.then((response)=>{
			if(response.ok){
				return response.text();
			}else{
				postReveal.postCnt.removeChild(postReveal.appendLoader());
				event.target.disabled = false;
				throw new Error('Bad HTTP');	
			}
		})
		.then((text)=>{
			if(text !== ''){
				let postsCnt = document.querySelector('.posts_cnt') || document.querySelector('.results');
				let newPageWithPosts = document.createElement('DIV');
				newPageWithPosts.innerHTML = text;
				let newPosts = newPageWithPosts.firstChild;
				if(dataPrev == 1){
                    postsCnt.insertBefore(newPosts, postsCnt.childNodes[0]);
                    event.target.setAttribute('data-number', Number(dataNumber) - 1);
				}else{
					postsCnt.appendChild(newPosts);
					event.target.setAttribute('data-number', Number(dataNumber) + 1);
				}
				
				return postsCnt;
			}
		})
		.then((newPostsAdded)=>{
			event.target.disabled = false;
			if(this.postCnt.querySelector('.loading')) postReveal.postCnt.removeChild(this.postCnt.querySelector('.loading'));
			if(newPostsAdded){
			    if(event.target.getAttribute('id') == 'load_previous'){
			    	if(event.target.getAttribute('data-number') == 1){
			    		topNotification.classList.add('reveal');
			    		event.target.disabled = true;
			    	}
			    	newPostsAdded.querySelector('.post_item:not(.added)').classList.add('added');
					mainCntScroll.addMomentum(0, 900);
		    		mainCntScroll.scrollIntoView(event.target, {
		    			offsetTop: 80
		    		});
		    		
			    }else{
			    	newPostsAdded.querySelector('.post_item:not(.added)').classList.add('added');
					mainCntScroll.addMomentum(0, 900);
			    	mainCntScroll.scrollIntoView(newPostsAdded.lastElementChild, {
			    		alignToTop: true,
			    		offsetTop: 80
			    	});
			    }
				mainCntScroll.update();
			}else{
				bottomNotification.classList.add('reveal');
				event.target.disabled = true;
			}
			
		})		
		.catch((err) =>{
			if(err.message){
                console.log(err.message);

                if(this.postCnt.querySelector('.loading')) postReveal.postCnt.removeChild(this.postCnt.querySelector('.loading'));

                if(event.target.getAttribute('id') == 'load_previous'){
			    	topNotification.innerHTML = 'Failed to load, pls refresh the page';
				    topNotification.classList.add('reveal');
			    }else{
			    	bottomNotification.innerHTML = 'Failed to load, pls refresh the page';
				    bottomNotification.classList.add('reveal');
			    }
                mainCntScroll.update();
			}else{
				console.log(err);
				mainCntScroll.update();	
			}
		});  
}

function updateUrl(st){
	let scroll = st.offset.y;
	let scrollBarSizeY = mainCntScroll.size.container.height;
	if( Math.abs( scroll - lastScroll ) > scrollBarSizeY * 0.1 ) {
        lastScroll = scroll;
    	Array.from(postPage).forEach((element)=>{
	    	if(isVisible(element)){
	    		history.replaceState(null, null, element.getAttribute('data-page'));
	    		return false;
	    	}
	    });
	}
}
function showHideGoTopBtn(st){
	let currentScroll = st.offset.y;
	if(Math.round(currentScroll) > Math.round(window.innerHeight) ){
		goTopBtn.classList.remove('hidden_g');
	}else{
		goTopBtn.classList.add('hidden_g');
	}

}
// underscore js part( to  throttle smooth scroll bar "scroll" event) start
let o = function(obj) {
	if (obj instanceof o) return obj;
	if (!(this instanceof o)) return new o(obj);
	this.owrapped = obj;
};
o.now = Date.now || function() {
	return new Date().getTime();
};
o.throttle = function(func, wait, options) {
	let timeout, context, args, result;
	let previous = 0;
	if (!options) options = {};

	let later = function() {
		previous = options.leading === false ? 0 : o.now();
		timeout = null;
		result = func.apply(context, args);
		if (!timeout) context = args = null;
	};

	let throttled = function() {
	  let now = o.now();
	  if (!previous && options.leading === false) previous = now;
	  let remaining = wait - (now - previous);
	  context = this;
	  args = arguments;
	  if (remaining <= 0 || remaining > wait) {
	    if (timeout) {
			clearTimeout(timeout);
			timeout = null;
	    }
	    previous = now;
	    result = func.apply(context, args);
	    if (!timeout) context = args = null;
	  } else if (!timeout && options.trailing !== false) {
	    timeout = setTimeout(later, remaining);
	  }
	  return result;
	};

	throttled.cancel = function() {
		clearTimeout(timeout);
		previous = 0;
		timeout = context = args = null;
	};

	return throttled;
};
// underscore js part( to  throttle smooth scroll bar "scroll" event) end
let throttled = o.throttle(updateUrl, 330, {leading: true});
let throttledScrollBtn = o.throttle(showHideGoTopBtn, 330, {leading: true});
window.addEventListener("load",() =>{
	mainCntScroll.addListener((status) => {
		throttled(status);
		throttledScrollBtn(status);
	});
});


function isVisible( el ){
	let scroll_pos = mainCntScroll.offset.y;
	let window_height = mainCntScroll.size.container.height;
	let el_top = el.offsetTop;
	let el_height = el.offsetHeight;
	let el_bottom = el_top + el_height;
	return ( ( el_bottom - el_height * 0.25 > scroll_pos ) && ( el_top < ( scroll_pos + 0.5 * window_height ) ) );	
}

// contact form start
if(contactForm){
	contactForm.addEventListener('submit', function(e){
		e.preventDefault();
		sendContactInfo(e);
	})
}
// contact form end
 
function sendContactInfo(event){
	const ajaxUrl = event.target.getAttribute('data-url');
	let form = event.target,
	name = document.getElementById('oc_name'),
	email = document.getElementById('oc_email'),
	message = document.getElementById('oc_message'),
	btn = form.querySelector('[type="submit"]'),
	formData = new FormData();
	formData.append('name', name.value);
	formData.append('email', email.value);
	formData.append('message', message.value);
	formData.append('action', 'oc_save_contact');

    if( name.value === '' ){
    	name.classList.add('has_error');
    	name.nextElementSibling.classList.add('text_danger');
		return;
	}else{
		name.classList.remove('has_error');
		name.nextElementSibling.classList.remove('text_danger');
	}

	if( email.value === '' ){
		email.classList.add('has_error');
		email.nextElementSibling.classList.add('text_danger');
		return;
	}else{
		email.classList.remove('has_error');
		email.nextElementSibling.classList.remove('text_danger');
	}

	if( message.value === '' ){
		message.classList.add('has_error');
		message.nextElementSibling.classList.add('text_danger');
		return;
	}else{
		message.classList.remove('has_error');
		message.nextElementSibling.classList.remove('text_danger');
	}

    name.disabled = true;
    email.disabled = true;
    message.disabled = true;
    btn.disabled = true;
    let options = {
    	method: 'POST',
    	body: formData
    };
    document.querySelector('.text_info').classList.add('js_form_submission');
	let req = new Request(ajaxUrl, options);
    
	fetch(req)
		.then((response)=>{
			if(response.ok){
				setTimeout(function(){
					document.querySelector('.text_info').classList.remove('js_form_submission');
					document.querySelector('.text_success').classList.add('js_form_success');
					form.reset();
				}, 1000);
			}else{
				throw new Error('Bad HTTP');	
			}
		})
		.catch((err) =>{
			if(err.message){
	            name.disabled = false;
			    email.disabled = false;
			    message.disabled = false;
			    document.querySelector('.text_info').classList.remove('js_form_submission');
			    document.querySelector('.text_err').classList.add('js_form_error');
			}else{
				name.disabled = false;
			    email.disabled = false;
			    message.disabled = false;
			    document.querySelector('.text_info').classList.remove('js_form_submission');
			    document.querySelector('.text_err').classList.add('js_form_error');	
			}
		});

}
// open close search start
if(searchOpenBtn){
	
	searchCloseBtn.addEventListener('click', (e) =>{
        searchCnt.classList.toggle('hidden_search', !searchCnt.classList.contains('hidden_search'));
        document.querySelector('.search_animate.revealed').classList.remove('revealed');
        document.removeEventListener('keydown', closeSearchOnEsc);
	});

	for (i = 0; i < searchOpenBtn.length; i++) {
		searchOpenBtn[i].addEventListener('click', (e) =>{
			e.preventDefault();
	        searchCnt.classList.toggle('hidden_search', !searchCnt.classList.contains('hidden_search'));
	        document.querySelector('.search_animate:not(.revealed)').classList.add('revealed');
	        document.addEventListener('keydown', closeSearchOnEsc);
		});
	}
}

function closeSearchOnEsc (evt){
    evt = evt || window.event;
    let isEscape = false;
    if("key" in evt){
        isEscape = (evt.key == "Escape" || evt.key == "Esc");
    }else{
        isEscape = (evt.keyCode == 27);
    }
    if(isEscape){
       searchCnt.classList.add('hidden_search');
       document.querySelector('.search_animate.revealed').classList.remove('revealed');
       document.removeEventListener('keydown', closeSearchOnEsc);
    } 
}
// open close search end

// mobile menu start
if(mobileMenuToggle) mobileMenuToggle.addEventListener('click', openCloseMenu);
if(mobileMenuClose) mobileMenuClose.addEventListener('click', openCloseMenu);

function openCloseMenu(e){
	e.stopPropagation();
	if(menuColumn.classList.contains('column_hidden')){
		mobileMenu.classList.remove('hidden');
		menuColumn.classList.remove('column_hidden');
		mobileMenuClose.classList.add('active');
	}else{
		mobileMenu.classList.add('hidden');
		menuColumn.classList.add('column_hidden');
		mobileMenuClose.classList.remove('active');
	}
}
if(scrollbarMenuCnt){
	menuScrollBar = Scrollbar.init(scrollbarMenuCnt, 
	    ScrollbarOptions = {
	    alwaysShowTracks: true,
	});
}

// media query
if(mobileMenu){
	function controllVpSize(x) {
		if ( x.matches && !menuColumn.classList.contains('column_hidden')) { // If media query matches
			mobileMenu.classList.add('hidden');
			menuColumn.classList.add('column_hidden');
			mobileMenuClose.classList.remove('active');
	    } 
	}

	let x = window.matchMedia("(min-width: 992px)");
	controllVpSize(x); // Call listener function at run time
	x.addListener(controllVpSize);
}
// media query end  

// mobile menu end

// scroll to top button start

goTopBtn.addEventListener('click', ()=>{
	mainCntScroll.scrollTo(0, 0, 1000);
});

// scroll to top button end
