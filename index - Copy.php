<?php
	get_header();
?>
<!-- <?php if(!is_paged()): ?>
<section class="container filter_cnt">
	<div class="row">
		<div class="col-12">
			<div class="d-flex flex-row justify-content-between align-items-center py-4 sorting_post_cnt">
				<?php
					
	                $args = array( 
	                	'post_type'=> 'post',
	                	'posts_per_page' => -1,
						'no_found_rows' => true, 
						'update_post_meta_cache' => false, 
						'update_post_term_cache' => false, 
						'fields' => 'ids'
					);

					$loop = new WP_Query( $args );

					$post_formats_arr = array();

					if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post();
						
						$post_formats = get_post_format() ? : 'standard';

	                    array_push($post_formats_arr, $post_formats);
	                    
					endwhile;		
					endif; 		
	                wp_reset_postdata();
	                $post_formats_arr_names = array_unique($post_formats_arr);
	            ?>  
				<ul class="sorting_post_cnt__item">
					<li class="d-flex justify-content-between align-items-center"> 
					    <span class="pr-5 py-2 sorting_lable">All:</span> <input type="radio" id="all" name="post-format" value="all" class="radio_sort" checked="checked"> 
				    </li>
	                <?php 
	                if (empty(!$post_formats_arr_names)) {
		                foreach ($post_formats_arr_names as $item) {
						    echo '<li class="d-flex justify-content-between align-items-center"> 
								    <span class="pr-5 py-2 sorting_lable">' . $item . ':</span> <input type="radio" id="'. $item .'" name="post-format" value="post-format-'. $item .'" class="radio_sort"> 
								  </li>';
						}
					}
					?>
				</ul>
				<ul>
	                <?php echo printf($post_formats_arr_names); ?>
				</ul>
				<div class="sorting_post_cnt__item">
					<button id="sort" data-url="<?php echo admin_url('admin-ajax.php'); ?>">Sort and show</button>
				</div>
			</div>
		</div>
	</div>
</section>
<?php endif; ?> -->
<section class="container main">
	<div class="row">
		<div class="col-12 position-relative">
        <?php if(is_paged()): ?>
			<div class="load_more_cnt load_more_cnt--previous">
		    	<button id="load_previous" class="load_button" data-prev="1" data-number="<?php echo if_paged(1); ?>" data-url="<?php echo admin_url('admin-ajax.php'); ?>">LOAD PREVIOUS</button>
		    </div>
        <?php endif; ?>
			<div class="posts_cnt">
				<?php echo '<div data-number="1" class="page" data-page="' . get_site_url(null , null , 'relative') . '/'. if_paged() .'">'; ?>
					<?php 
					if ( have_posts() ) {
						while ( have_posts() ) {

							the_post();					        
							get_template_part( 'template-parts/one', 'post' ); 
							$post_cat = get_the_terms( $post->ID, 'category' );
				         	print_r($post_cat[0]);
				         	foreach ($post_cat as $cat) {
				         		echo $cat->name;
				         	}
				         	unset($cat);
				        	
						} // end while
					} // end if
					?>

				<?php echo '</div>'; ?>
			</div>
			
		    <div class="load_more_cnt">
		    	<span class="rocbot">Rock Bottom</span>
		    	<button id="load_posts" class="load_button" data-number="<?php echo if_paged(1); ?>" data-url="<?php echo admin_url('admin-ajax.php'); ?>">LOAD MORE</button>
		    </div>
	    </div>
    </div>
</section>

<section class="container">
	<div class="row">
		<div class="col-12">
			contact
		</div>
	</div>
</section>

<?php		
	get_footer();
?>