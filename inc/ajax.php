<?php
	add_action('wp_ajax_nopriv_first_fetch_f', 'first_fetch_f');
	add_action('wp_ajax_first_fetch_f', 'first_fetch_f');
	function first_fetch_f(){
		$num = wp_strip_all_tags( $_POST["number"] );
		$numPrev = wp_strip_all_tags( $_POST["prev"] );
		$num_p = ( $num == (int)$num ) ? (int)$num + 1 : 1;
		$search = wp_strip_all_tags($_POST["search"]);
		$archive = wp_strip_all_tags($_POST["archive"]);
		if($numPrev == 1 && $num != 1){
			$num_p = ( $num == (int)$num ) ? (int)$num - 1 : 1;
		}

		if( $archive != '0' ){
			
			$archVal = explode( '/', $archive );
			
			if( in_array( "category", $archVal ) ){
				
				$type = "category_name";
				$currKey = array_keys( $archVal, "category" );
				$nextKey = $currKey[0]+1;
				$value = $archVal[ $nextKey ];
				
				$args[ $type ] = $value;
				
			}
			
			if( in_array( "tag", $archVal ) ){
				
				$type = "tag";
				$currKey = array_keys( $archVal, "tag" );
				$nextKey = $currKey[0]+1;
				$value = $archVal[ $nextKey ];
				
				$args[ $type ] = $value;
				
			}
			
			if( in_array( "author", $archVal ) ){
				
				$type = "author";
				$currKey = array_keys( $archVal, "author" );
				$nextKey = $currKey[0]+1;
				$value = $archVal[ $nextKey ];
				
				$args[ $type ] = $value;
				
			}
			
			//check page trail and remove "page" value
			if( in_array( "page", $archVal ) ){
				
				$pageVal = explode( 'page', $archive );
				$page_trail = $pageVal[0];
				
			} else {
				$page_trail = $archive;
			}
			
		} else {
			$page_trail =  get_site_url(null , null , 'relative') .'/';
		}
        if($search !== '0'){
        	$searhQuery = new WP_Query(
	        	array(
	        		'post_type'   => 'post',
	        		'post_status' => 'publish',
	        		'paged'       => $num_p,
			        's'           => $search
	        	)
	        );

	        if($searhQuery->have_posts()):
	        	echo '<div data-number="'. $num_p .'" class="post_item" data-page="' . get_site_url(null , null , 'relative') . '/page/'. $num_p .'/?s='. $search .'&submit=' . '">';
		        while($searhQuery->have_posts()): $searhQuery->the_post(); 
		        	get_template_part( 'template-parts/one', 'post' );
				endwhile;
				echo '</div>';

			endif;

        }else{
        	$query = new WP_Query(
	        	array(
	        		'post_type' => 'post',
	        		'post_status' => 'publish',
	        		'paged' => $num_p
	        	)
	        );
	        if($query->have_posts()):
	        	echo '<div data-number="'. $num_p .'" class="post_item" data-page="' . $page_trail . 'page/'. $num_p . '">';
		        while($query->have_posts()): $query->the_post(); 
		        	get_template_part( 'template-parts/one', 'post' );
				endwhile;
				echo '</div>';

			endif;
        }
		wp_reset_postdata();
        
		wp_die();
	}
	function if_paged($num = null){
		$output = '';
		if(is_paged()){ $output = 'page/'. get_query_var('paged');}

		if($num == 1){
			$num_p = ( get_query_var('paged') == 0 ? 1 : get_query_var('paged') );
            return $num_p;
		}else{
			return $output;
		}
		
	}
     
	// save contact form values

	add_action('wp_ajax_nopriv_oc_save_contact', 'oc_save_contact');
	add_action('wp_ajax_oc_save_contact', 'oc_save_contact');


	function oc_save_contact(){
	    $title = wp_strip_all_tags($_POST['name']);
	    $email = wp_strip_all_tags($_POST['email']);
	    $message = wp_strip_all_tags($_POST['message']);
	    $args = array(
	        'post_title' => $title,
	        'post_content' => $message,
	        'post_author' => 1,
	        'post_status' => 'publish',
	        'post_type' => 'oc-contact',
	        'meta_input' => array(
	            '_contact_email_value_key' => $email,
	        ),
	    );
	    $postID = wp_insert_post($args);

	    if($postID !== 0){
	    	$to = get_bloginfo('admin_email');
	    	$subject  = 'One Column Contact Form - '. $title;

	    	$headers[] = 'From: '. get_bloginfo('name') . '<'. $to .'>';
	    	$headers[] = 'Reply-To: '. $title . '<'. $email .'>';
	    	$headers[] = 'Content-Type: text/html; chartset=UTF-8';

	    	wp_mail($to, $subject, $message, $headers);
	    	echo $postID;
	    }else{
	    	echo 0;
	    }
	    
	    wp_die();
	}
