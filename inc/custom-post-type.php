<?php
	 
	$contact = get_option( 'activate_contact' );
	if( @$contact == 1 ){
		
		add_action( 'init', 'oc_contact_custom_post_type' );
		add_filter( 'manage_oc-contact_posts_columns', 'oc_set_contact_columns' );
		add_action( 'manage_oc-contact_posts_custom_column', 'oc_contact_custom_column', 10, 2 );
		
		add_action( 'add_meta_boxes', 'oc_contact_add_meta_box' );
		add_action( 'save_post', 'oc_save_contact_email_data' );
		
	}
	/* CONTACT CPT */
	function oc_contact_custom_post_type() {
		$labels = array(
			'name' 				=> 'Messages',
			'singular_name' 	=> 'Message',
			'menu_name'			=> 'Messages',
			'name_admin_bar'	=> 'Message'
		);
		
		$args = array(
			'labels'			=> $labels,
			'show_ui'			=> true,
			'show_in_menu'		=> true,
			'capability_type'	=> 'post',
			'hierarchical'		=> false,
			'menu_position'		=> 26,
			'menu_icon'			=> 'dashicons-email-alt',
			'supports'			=> array( 'title', 'editor', 'author' )
		);
		
		register_post_type( 'oc-contact', $args );
		
	}
	function oc_set_contact_columns( $columns ){
		$newColumns = array();
		$newColumns['title'] = 'Name';
		$newColumns['message'] = 'Message';
		$newColumns['email'] = 'Email';
		$newColumns['date'] = 'Date';
		return $newColumns;
	}
	function oc_contact_custom_column( $column, $post_id ){
		
		switch( $column ){
			
			case 'message' :
				echo get_the_excerpt();
				break;
				
			case 'email' :
				//email column
				$email = get_post_meta( $post_id, '_contact_email_value_key', true );
				echo '<a href="mailto:'. $email .'">'. $email .'</a>';
				break;
		}
		
	}
	/* CONTACT META BOXES */
	function oc_contact_add_meta_box() {
		add_meta_box( 'contact_email', 'User Email', 'oc_contact_email_callback', 'oc-contact', 'side' );
	}
	function oc_contact_email_callback( $post ) {
		wp_nonce_field( 'oc_save_contact_email_data', 'oc_contact_email_meta_box_nonce' );
		
		$value = get_post_meta( $post->ID, '_contact_email_value_key', true );
		
		echo '<label for="oc_contact_email_field">User Email Address: </label>';
		echo '<input type="email" id="oc_contact_email_field" name="oc_contact_email_field" value="' . esc_attr( $value ) . '" size="25" />';
	}
	function oc_save_contact_email_data( $post_id ) {
		
		if( ! isset( $_POST['oc_contact_email_meta_box_nonce'] ) ){
			return;
		}
		
		if( ! wp_verify_nonce( $_POST['oc_contact_email_meta_box_nonce'], 'oc_save_contact_email_data') ) {
			return;
		}
		
		if( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ){
			return;
		}
		
		if( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}
		
		if( ! isset( $_POST['oc_contact_email_field'] ) ) {
			return;
		}
		
		$my_data = sanitize_text_field( $_POST['oc_contact_email_field'] );
		
		update_post_meta( $post_id, '_contact_email_value_key', $my_data );
		
	}
