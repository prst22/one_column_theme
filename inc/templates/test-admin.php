<h1>One column theme custom settings</h1>
<h2>Test Contact Form</h2>
<?php settings_errors(); ?>

<p>Use this <strong>shortcode</strong> to activate the Contact Form inside a Page or a Post</p>
<p><code>[contact_form]</code></p>

<form method="post" action="options.php" class="sunset-general-form">
	<?php settings_fields( 'oc-contact-options' ); ?>
	<?php do_settings_sections( 'one_column_theme_contact' ); ?>
	<?php submit_button(); ?>
</form>