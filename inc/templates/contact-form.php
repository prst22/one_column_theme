<form id="one_column_cf" class="mx-auto one_column_comtact_form" action="#" method="post" data-url="<?php echo admin_url('admin-ajax.php'); ?>">

	<div class="form_group">
		<input type="text" class="form_field one_column_form_control" placeholder="Your Name" id="oc_name" name="name" autocomplete="off">
		<small class="form_control_msg">Your Name is Required</small>
	</div>

	<div class="form_group">
		<input type="email" class="form_field one_column_form_control" placeholder="Your Email" id="oc_email" name="email" autocomplete="off">
		<small class="form_control_msg">Your Email is Required</small>
	</div>

	<div class="form_group">
		<textarea name="message" id="oc_message" class="form_field one_column_form_control" placeholder="Your Message" rows="11" cols="100"></textarea>
		<small class="form_control_msg">A Message is Required</small>
	</div>
	
	<div class="d-flex justify-content-center align-items-center flex-column">
		<button type="submit" class="btn">Submit</button>
		<small class="text_info form_control_msg">Submission in process, please wait..</small>
		<small class="text_success form_control_msg">Message successfully submitted, thank you!</small>
		<small class="text_err form_control_msg">There was a problem with the contact form, please try again!</small>
	</div>

</form>