<?php  
	function one_col_customizer_register($wp_customize){
		$wp_customize->add_section('site_logo',array(
			'title'				=> __('Site logo', 'oc-theme'),
			'description'		=> sprintf(__('Site logo', 'oc-theme')),
			'priority'			=> 10
		));
		$wp_customize-> add_setting('logo', 
			array(
				'default'		=> _x( get_template_directory_uri() . '/inc/images/logo.png', 'oc-theme'),
				'type' 			=> 'theme_mod',
				'capability'    => 'edit_theme_options',
			));
		$wp_customize->add_control(new WP_Customize_Image_Control( $wp_customize, 'logo', array(
				'label' => __( 'Header site logo', 'oc-theme' ),
				'section' => 'site_logo',
				'settings' => 'logo',
			)
		));
	}
	add_action('customize_register', 'one_col_customizer_register');