<?php get_header();?>
    <main class="container main">
        <div class="row">
            <div class="col-12">
                <div class="single_page single_page_to_animate">
                    <?php while ( have_posts() ) : the_post(); ?>
                     
                        <div class="single_page__post_heading">
                            <h1 class="mt-5 mb-3 text-center heading_title"><?php the_title(); ?></h1>
                        </div>
                        <div class="content_here">
                            <?php  the_content();  ?>
                        </div>

                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    </main>
<?php get_footer();?>