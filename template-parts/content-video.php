<?php
/**
 * Template part for displaying video posts
 *
 */
?>
<article <?php post_class( array( 'single_post', 'row') ); ?>>
	<div class="col-12 mt-5 mb-3">
		<h1><?php the_title(); ?></h1>
		<?php the_content(); ?>
	</div>
	
</article>