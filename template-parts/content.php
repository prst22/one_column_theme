<?php
/**
 * Template part for displaying posts
 *
 */
?>
<article <?php post_class( array( 'single_post', 'row' ) ); ?>>

	<div class="col-12 mt-5 mb-3">
		<h1><?php the_title(); ?></h1>
		<?php if(has_post_thumbnail()): ?>
		    <div class="mb-3">
			<?php the_post_thumbnail('large-thumnail', 
				$attr = array('class' => "single_post_thumbnail")); ?>
			</div>

		<?php endif;?>
		<?php the_content(); ?>
	</div>
	
</article>

