<?php
/**
 * Template part for displaying quote posts
 *
 */
?>
<article <?php post_class( array( 'single_post', 'row') ); ?>>
	
	<div class="col-12 mt-5 mb-3">
		<div class="quote_single">
		<span class="quotes_post--quote_left">
			<svg class="icon icon-quotes-left"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-quotes-left"></use></svg>
		</span>

		<h4 class="text-center"><?php the_content(); ?></h4>

		<span class="quotes_post--quote_right">
			<svg class="icon icon-quotes-right"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-quotes-right"></use></svg>
		</span>
		</div>
	</div>
		
</article>