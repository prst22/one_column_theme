<?php 
	$post_format = get_post_format();
	switch ($post_format ) {

		case 'video':?>
			<article <?php post_class( 'index_post' ); ?>>
				<h1 class="index_post__heading">
					<a href="<?php the_permalink() ?>" title="Link to <?php the_title_attribute(); ?>">
						<?php the_title(); ?>
					</a>
				</h1>
				<div class="d-flex justify-content-between index_post__info">
					<div>
						<span class="com_inf">
							<svg class="icon icon-eye"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-eye"></use></svg>
						</span>
						<span class="com_inf--num">
							<?php 
								$views = get_post_meta(get_the_ID(), 'oc_theme_post_views', TRUE);
								$print_views = empty($views) ? 0 : $views;
								echo $print_views;
							?>
						</span>
					</div>
					<div>
						<span class="com_inf">
							<svg class="icon icon-chat_bubble_outline"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-chat_bubble_outline"></use></svg>
						</span>
						<span class="com_inf--num">
							<?php echo get_comments_number(); ?>
						</span>
					</div>
				</div>
				<?php if(has_post_thumbnail()): ?>
					<figure class="thumbnail_cnt video_thumbnail_cnt mb-3">
						<a href="<?php the_permalink() ?>" title="Link to <?php the_title_attribute(); ?>" class="thumbnail_cnt__link video_thumbnail_cnt__link ">
							<?php the_post_thumbnail('large-thumnail', 
								$attr = array('class' => "video_thumbnail")); ?>
							<span class="video_play_btn">
								PLAY
								<i>
									<svg class="icon play-circle"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#play-circle"></use></svg>
								</i>
							</span>
                        </a>   
					</figure> 
				<?php endif; ?>
				<?php if( get_the_excerpt() != ''): ?>
					<div class="index_post_excerpt">
						<?php the_excerpt(); ?>
					</div>
				<?php endif; ?>
			</article>
		<?php break;

		case 'link':?>
			<article <?php post_class( array('index_post', 'index_link') ); ?>>

				<div class="d-flex justify-content-between index_post__info">
					<div>
						<span class="com_inf">
							<svg class="icon icon-eye"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-eye"></use></svg>
						</span>
						<span class="com_inf--num">
							<?php 
								$views = get_post_meta(get_the_ID(), 'oc_theme_post_views', TRUE);
								$print_views = empty($views) ? 0 : $views;
								echo $print_views;
							?>
						</span>
					</div>
					<div>
						<span class="com_inf">
							<svg class="icon icon-chat_bubble_outline"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-chat_bubble_outline"></use></svg>
						</span>
						<span class="com_inf--num">
							<?php echo get_comments_number(); ?>
						</span>
					</div>
				</div>

				<h1 class="index_post__heading d-flex justify-content-center align-items-center">
					<a href="<?php echo wp_strip_all_tags(get_the_content()); ?>" title="Link to <?php the_title_attribute(); ?>" class="d-flex flex-column align-items-center">
						<span><?php the_title(); ?></span>
						<span class="d-inline-block mt-5 index_link__icon">
							<svg class="icon link"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#link"></use></svg>
						</span>
					</a>
				</h1>
				<a href="<?php the_permalink() ?>" title="Link to <?php the_title_attribute(); ?>" class="link">
					View comments
				</a>
			</article>
		<?php break;

		case 'quote':?>
			<article <?php post_class( array('index_post', 'quotes_post') ); ?>>
				<div class="d-flex justify-content-between index_post__info">
					<div>
						<span class="com_inf">
							<svg class="icon icon-eye"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-eye"></use></svg>
						</span>
						<span class="com_inf--num">
							<?php 
								$views = get_post_meta(get_the_ID(), 'oc_theme_post_views', TRUE);
								$print_views = empty($views) ? 0 : $views;
								echo $print_views;
							?>
						</span>
					</div>
					<div>
						<span class="com_inf">
							<svg class="icon icon-chat_bubble_outline"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-chat_bubble_outline"></use></svg>
						</span>
						<span class="com_inf--num">
							<?php echo get_comments_number(); ?>
						</span>
					</div>
				</div>
				<div class="quotes_post__inner">
					<span class="quotes_post quotes_post--quote_left">
						<svg class="icon icon-quotes-left"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-quotes-left"></use></svg>
					</span>	

					<div class="my-5 quotes_post__content">
						<a href="<?php the_permalink() ?>" title="Link to <?php the_title_attribute(); ?>"><?php the_content(); ?>
						</a>
					</div>
					
					<span class="quotes_post quotes_post--quote_right">
						<svg class="icon icon-quotes-right"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-quotes-right"></use></svg>
					</span>	
				</div>
			</article>
		<?php break;

		default:?>
			<article <?php post_class( 'index_post' ); ?>>
				<h1 class="index_post__heading">
					<a href="<?php the_permalink() ?>" title="Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a>
				</h1>
				<div class="d-flex justify-content-between index_post__info">
					<div>
						<span class="com_inf">
							<svg class="icon icon-eye"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-eye"></use></svg>
						</span>
						<span class="com_inf--num">
							<?php 
								$views = get_post_meta(get_the_ID(), 'oc_theme_post_views', TRUE);
								$print_views = empty($views) ? 0 : $views;
								echo $print_views;
							?>
						</span>
					</div>
					<div>
						<span class="com_inf">
							<svg class="icon icon-chat_bubble_outline"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-chat_bubble_outline"></use></svg>
						</span>
						<span class="com_inf--num">
							<?php echo get_comments_number(); ?>
						</span>
					</div>
				</div>
				<?php if(has_post_thumbnail()): ?>
					<figure class="thumbnail_cnt mb-3">
						<a href="<?php the_permalink() ?>" title="Link to <?php the_title_attribute(); ?>" class="thumbnail_cnt__link video_thumbnail_cnt__link ">
							<?php the_post_thumbnail('large-thumnail', 
								$attr = array('class' => "video_thumbnail")); ?>
                        </a>   
					</figure> 
				<?php endif;?>
				<div class="index_post__excerpt">
					<?php the_excerpt(); ?>
				</div>
			</article>
		<?php break;
	}
	