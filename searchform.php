<form method="get" class="searchform form-inline text-center search_form" autocomplete="off" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<input type="text" class="mt-3 search_field" name="s" id="s" placeholder="<?php esc_attr_e( 'Search' ); ?>" title="search" size="15"/>
	<button class="mt-3 btn" type="submit" name="submit" id="searchsubmit">
		<!-- <svg class="icon search"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#search"></use></svg> -->
		Search
	</button>
</form>