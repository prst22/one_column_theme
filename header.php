<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="description" content="<?php bloginfo('description'); ?>">
	<title>
		<?php bloginfo('name');?> |
		<?php is_front_page() ? bloginfo('description') : wp_title('');?> 
	</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="author" content="Igor Barabash">
	<meta name="keywords" content="Custom wordpress theme">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri(); ?>/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_stylesheet_directory_uri(); ?>/site.webmanifest">
	<link rel="mask-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">

	<style>
		.circular,.pre_loader{top:0;bottom:0;left:0;right:0}.pre_loader{visibility:visible;position:fixed;z-index:1000;opacity:1;display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center;-webkit-box-align:center;-ms-flex-align:center;align-items:center;background:-webkit-linear-gradient(183deg,#7934f7,#755bf9 50%,#7282fb);background:-o-linear-gradient(183deg,#7934f7,#755bf9 50%,#7282fb);background:linear-gradient(267deg,#7934f7,#755bf9 50%,#7282fb);-webkit-transition:all .6s cubic-bezier(.445,.050,.55,.95);-o-transition:all .6s cubic-bezier(.445,.050,.55,.95);transition:all .6s cubic-bezier(.445,.050,.55,.95)}.pre_loader__spinner{width:200px;height:200px}.pre_loader.hidden{visibility:hidden;opacity:0;pointer-events:none}.circular{-webkit-animation:rotate 2s linear infinite;animation:rotate 2s linear infinite;height:100%;-webkit-transform-origin:center center;transform-origin:center center;width:100%;position:absolute;margin:auto}.path{stroke-dasharray:1,200;stroke-dashoffset:0;-webkit-animation:dash 1.5s ease-in-out infinite,color 6s ease-in-out infinite;animation:dash 1.5s ease-in-out infinite,color 6s ease-in-out infinite;stroke-linecap:round}@-webkit-keyframes rotate{100%{-webkit-transform:rotate(360deg);transform:rotate(360deg)}}@keyframes rotate{100%{-webkit-transform:rotate(360deg);transform:rotate(360deg)}}@-webkit-keyframes dash{0%{stroke-dasharray:1,200;stroke-dashoffset:0}50%{stroke-dasharray:89,200;stroke-dashoffset:-35px}100%{stroke-dasharray:89,200;stroke-dashoffset:-124px}}@keyframes dash{0%{stroke-dasharray:1,200;stroke-dashoffset:0}50%{stroke-dasharray:89,200;stroke-dashoffset:-35px}100%{stroke-dasharray:89,200;stroke-dashoffset:-124px}}@-webkit-keyframes color{0%,100%{stroke:#a8eb12}40%{stroke:#e83e8c}66%{stroke:#007bff}80%,90%{stroke:#FF6F59}}@keyframes color{0%,100%{stroke:#a8eb12}40%{stroke:#e83e8c}66%{stroke:#007bff}80%,90%{stroke:#FF6F59}}
	</style>
	<?php wp_head(); ?>
</head>

<body <?php body_class();?>>
	<section class="pre_loader">
		<div class="pre_loader__spinner">
			 <svg class="circular" viewBox="100 100 200 200">
				<circle class="path" cx="200" cy="200" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
			</svg>
		</div>
	</section>
	<section class="d-flex justify-content-center align-items-center search_block hidden_search" id="search_block">
		<div class="search_block__close_search_cnt">
			<div class="cross" id="close_search">
				<div class="bar"></div>
				<div class="bar"></div>
			</div>
		</div>
		<div class="search_block_inner  search_animate">
	    	<?php get_search_form(); ?>
    	</div>	
	</section>
	<?php if ( has_nav_menu( 'main_menu' ) ) : ?>
	<section class="mobile_menu_wrapper">

		<nav class="mobile_menu_cnt hidden">
			
			<div class="mobile_menu_cnt__inner column_hidden">
				<div class="menu_content mb-5">
	                <div class="close_menu_cnt">
						<div class="cross cross--close" id="close_menu">
							<div class="bar"></div>
							<div class="bar"></div>
						</div>
	                </div>
					<?php
					   wp_nav_menu( array(
						   'menu'              => 'Main menu',
						   'theme_location'    => 'main_menu',
						   'menu_class'        => 'mobile_menu_list_cnt__mobile_list',
						   'depth'             => 1, 
						   'container_class'   => 'mobile_menu_list_cnt',                  
						   )
					   );
					?>
				</div>
			</div>
		</nav>

	</section> 
	<?php endif; ?>
	<div class="site_wrap">
		<div class="site_wrap__inner">
			<section class="top">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<header class="d-flex justify-content-center align-items-center site_header">
								<?php if (get_theme_mod('logo') !== ''): ?>
									<a href="<?php echo home_url();?>" alt="Home">
										<img src="<?php echo get_theme_mod('logo'); ?>" alt="<?php bloginfo('name');?>">
									</a>
								<?php else: ?>
									<h1 class="main_site_header">
										<a href="<?php echo home_url();?>" title="Go home">
											<?php bloginfo('name'); ?>
										</a>
									</h1>
								<?php endif; ?>	
							</header>
						</div>
						<?php if ( has_nav_menu( 'main_menu' ) ) : ?>
						<div class="col-12">
                            <div class="mobile_menu_toggle">
							    <div class="mobile_menu_toggle__button" id="mobile_menu_btn">
									<span></span>
									<span></span>
							    </div>
							</div>

							<nav class="d-lg-flex justify-content-center align-items-center nav">
								
								<?php
								   wp_nav_menu( array(
									   'menu'              => 'Main menu',
									   'theme_location'    => 'main_menu',
									   'menu_class'        => 'd-md-inline-flex nav_list',
									   'depth'             => 1,          
									   'container_class'   => 'nav__inner',           
									   )
								   );
								?>
							</nav>

						</div>
						<?php endif; ?>
					</div>
				</div>
				<div class="gradient"></div>
			</section>
		
	
	