<?php
    require get_template_directory() . '/inc/ajax.php';
    require get_template_directory() . '/inc/customizer.php';
    require get_template_directory() . '/inc/function-admin.php';
    require get_template_directory() . '/inc/custom-post-type.php';
    require get_template_directory() . '/inc/shortcodes.php';

    //add theme support start

    function theme_setup() {
    	load_theme_textdomain( 'oc-theme' );

    	// Adding menus to wp controll pannel
		register_nav_menus(array(
			'main_menu' =>__('Main menu'),
			'footer_menu' =>__('Footer Menu')
		));

	    //post thum pictures
	    add_theme_support('post-thumbnails');

	    add_image_size('small-thumnail', 180, 101, array('center','center'));
	    add_image_size('medium-thumnail', 500, 281, array('center','center'));  
	    add_image_size('large-thumnail', 1110, 625, array('center','center'));
	   
	    add_theme_support( 'post-formats', array( 'video', 'link', 'quote' ));

    	//CUSTOM WORDPRESS EDITOR STYLE
		add_editor_style( 'css/custom-editor-styles.min.css' );
	    //CUSTOM WORDPRESS EDITOR STYLE 
	    add_theme_support( 'customize-selective-refresh-widgets' );

    }
    add_action( 'after_setup_theme', 'theme_setup' );

	//add theme support end


    //adding css styles start
	function add_styles_js(){
		wp_enqueue_style('style', get_theme_file_uri( '/css/main.css'));
		wp_enqueue_script('main_js', get_template_directory_uri() . '/js/main.min.js', array(), 1.0, true);
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
	}
	add_action('wp_enqueue_scripts', 'add_styles_js');
	 //adding css styles end

	//Controll post length on the main page

	function set_excerpt_length(){
		if(is_front_page()){
			return has_post_thumbnail() ? 25 : 50;
		}
		else if(is_page('all-news')){
			return has_post_thumbnail() ? 65 : 80;
		}else{
			return has_post_thumbnail() ? 40 : 75;
		} 
	}
	add_filter('excerpt_length','set_excerpt_length');

	function custom_excerpt_read_more( $more ) {
		global $post;
	    return sprintf( '<a class="read-more" href="%1$s">%2$s</a>',
	        get_permalink( get_the_ID() ),
	        __( '<span class="read-more__icon company comp-circle-right"></span>', 'oc-theme' )
	    );
	}
	add_filter( 'excerpt_more', 'custom_excerpt_read_more' );

	//widgets locations start
	function wpb_init_widgets($id){
		register_sidebar(array(
		    'name'         => 'footer_widget',
		    'id'           => 'footer_widget',
		    'before_widget'=> '<div class="footer_content__footer_widget">',
		    'after_widget' => '</div>'
		));
	}
	add_action('widgets_init', 'wpb_init_widgets');
	//widgets locations end

	//search input start
    function wpdocs_after_setup_theme() {
	    add_theme_support( 'html5', array( 'search-form' ) );
	}
	add_action( 'after_setup_theme', 'wpdocs_after_setup_theme' );
    //search input end

	// Add responsive container to embeds
 
	function embed_html( $html ) {
	   return '<div class="video_content">' . $html . '</div>';
	}
	add_filter( 'embed_oembed_html', 'embed_html', 10, 3 );

	// removing url field in comments section start
	function disable_url_in_comments_field($fields){
		if(isset($fields['url']))
		unset($fields['url']);
		return $fields;
	}
    add_filter('comment_form_default_fields', 'disable_url_in_comments_field');
    
	// First, create a function that includes the path to your favicon
	function add_favicon() {
	  	$favicon_url = get_template_directory_uri() . '/admin-favicon.ico';
		echo '<link rel="shortcut icon" href="' . $favicon_url . '" type="image/x-icon" sizes="32x32" />';
	}
	  
	// Now, just make sure that function runs when you're on the login page and admin pages  
	add_action('login_head', 'add_favicon');
	add_action('admin_head', 'add_favicon');
    // count amount of views of a post start
    function save_post_views( $postID ) {
		
		$metaKey = 'oc_theme_post_views';
		$views = get_post_meta( $postID, $metaKey, true );
		
		$count = ( empty( $views ) ? 0 : $views );
		$count++;
		
		update_post_meta( $postID, $metaKey, $count );
		
	}
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
    // count amount of views of a post end
    
    //exclude pages from search
    function my_post_queries( $query ) {
		// do not alter the query on wp-admin pages and only alter it if it's the main query
		if (!is_admin() && $query->is_main_query()){

			if (is_search()) {
				$query->set('post_type', array('post'));
			}
		}
	}
	add_action( 'pre_get_posts', 'my_post_queries' );

    // get url for archive page

	function current_uri() {
		$http = ( isset( $_SERVER["HTTPS"] ) ? 'https://' : 'http://' );
		$referer = $http . $_SERVER["HTTP_HOST"];
		$archive_url = $referer . $_SERVER["REQUEST_URI"];
		
		return $archive_url;
	}
	